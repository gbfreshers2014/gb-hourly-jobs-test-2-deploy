#!/bin/bash
# generateBranch.sh: a sample shell script to automate the branching process partially. You are welcome to modify this script to fix any error or make improvements.
# Input:
# - Project name (Should be exactly same as provided in svn repo)
# - Branch version (For example 1.8, 1.23 etc )
# - Bug number (JIRA Bug number like BACK-007)
# 
# Work Flow:
# - Create branch for project with appropriate version
# - Svn up code on local machine
# - Remove SnapShot from branch poms
# - Commit the code
# - Update Externals
#
# What is does not do:
# - Bump up the version in trunk
# - Bump up minor version in case some code is changed in branch

##########################################################################################
#######################   Global variables declaration Here !!!   ########################
##########################################################################################

readonly BASE_PROJECT="BI"
readonly SVN_BASE_REPO="https://gwynniebee.atlassian.net/svn"
readonly SVN_REPO="$SVN_BASE_REPO/$BASE_PROJECT"
readonly TRUNK_DIR_NAME="trunk"
readonly BRANCH_DIR_NAME="branches"

readonly NOW=$(date +"%m-%d-%Y-%H-%M-%S")
readonly LOG_FILE="/tmp/GenerateBranch-${BASE_PROJECT}-${NOW}.log"

readonly STATUS_TAG="STATUS"
readonly INFO_TAG="INFO"
readonly ERROR_TAG="ERROR"

readonly RUN_TYPE_COMMIT="commit"
readonly RUN_TYPE_NO_COMMIT="no-commit"

readonly SVN_COMMAND_UP="UP"
readonly SVN_COMMAND_CI="CI"

readonly RUN_TYPES=( commit no-commit )
readonly BOOLEAN_TYPES=( true false )

##########################################################################################
###########################   Function Definitions Here !!!   ###########################
##########################################################################################
# Help function for outputting usages details of script
helpUsage(){
        echo "Usage: $0 projectName branchVersion jiraTicket runType{no-commit|commit} quietMode{true|false} trunkNewVersion{1.2|1.4|1.24 ...}"
}

# Main log function. It logs on screen as well as logFile
log(){
	echo -e "${*}"
	echo -e "${*}" >> ${LOG_FILE}
}

# Log info message with dates.
logInfo(){
	if [ "$1" == "false" ]; then
		log "[`date`] - $INFO_TAG - ${2}"
	fi
}

# Log status message with dates.
logStatus(){
	log "[`date`] - $STATUS_TAG - ${*}"
}

# Log error message with dates.
logError(){
	log "[`date`] - $ERROR_TAG - ${*}"
}

# Insert a new line
logNewLine(){
	log "\n"
}

# Log header information
logHeader(){
	log "###############################################################################"
	log "${*}"
	log "###############################################################################"
}

#Executing svn command
executeSvnCommand(){
	local runType="$1"
	local quietMode="$2"
	local command="$3"
	local message="$4"
	local execute=""
	local returnStatus=1;
	
	case  "$command"  in
		"$SVN_COMMAND_UP")
			execute="svn up --ignore-externals"
			if [ "$quietMode" == "true" ]; then
				execute="svn up --ignore-externals --quiet"
			fi
			
			logInfo $quietMode "$execute"
		    $execute
			returnStatus="$?"
		;;
		"$SVN_COMMAND_CI")
			if [ "$runType" == "$RUN_TYPE_COMMIT" ]; then

				if [ "$quietMode" == "true" ]; then
					logInfo $quietMode "svn ci --quiet -m\"$message\""
					svn ci --quiet -m"$message"
				else
					logInfo $quietMode "svn ci -m\"$message\""
					svn ci -m"$message"
				fi
				
				returnStatus="$?"				
			else
				returnStatus="100"
			fi
		;;
		*)
			echo "Unknown command, this script should not have come here. Contact Author"
			quitBranchingScript
		;;            
    esac
    
    return "$returnStatus"
}

# Validate SVN Repository.
# Script should be executed from base svn repository folder, for example:
# https://gwynniebee.atlassian.net/svn/BACK or https://gwynniebee.atlassian.net/svn/GBEE
#
# If its executed from other location than script will quit.
validateSvnRepo(){
    local svnRepo=`svn info | grep URL | cut -d " " -f 2`
	if [ "$svnRepo" == "$SVN_REPO" ]; then
		return 0
	else
		logNewLine
		logError "You have to execute this script from $SVN_REPO [Current: $svnRepo]"
        return 1
	fi
}

# Quit branching script 
quitBranchingScript(){
	logNewLine
	log "Quitting branching script. Bye"
	exit 1
}

# Check if a value given for param is allowed or not.
# Note this validation is only done for runType, quietMode, trunkVersionUp
# $1 - param value
# $2 - array values
# TODO: Current implementation is using substring comparison. This should be done by checking element exist in array/set
paramValueValidation(){
	local validationStatus=1
	local paramName=$1
	local paramValue=$2
	local allowedValues=$3
	if [ "$allowedValues" != "${allowedValues/$paramValue/}" ]; then
  		validationStatus=0
  	else
  		validationStatus=1
	fi
	
	if [ "$validationStatus" -eq "0" ]; then
		logStatus "$paramName argument varification... Passed"
	else
		logStatus "$paramName argument varification... Failed"
		logError "$paramName argument can only have {$allowedValues} value"
		helpUsage
		quitBranchingScript
	fi
}

#Remove SNAPSHOT from branch pom files
removeSnapShot(){
	for pomFile in `find $2 | grep "pom\.xml$"`
	do
		logInfo $1 "Removing -SNAPSHOT from $pomFile"
		sed 's/-SNAPSHOT//' $pomFile >$pomFile.new
		mv $pomFile.new $pomFile
	done
}

bumpUpVersion(){
	for pomFile in `find $2 | grep "pom\.xml$"`
	do
		logInfo $1 "Bump up version of $pomFile"
		local branchVersion="$4.0"
		local trunkVersion="$3.0"
		sed -e "s/$branchVersion/$trunkVersion/" $pomFile >$pomFile.new
		mv $pomFile.new $pomFile
	done
}

##########################################################################################
#######################   Starting execution of script Here !!!   ########################
##########################################################################################

# Checking command line parameters, Call helpUsage() function if any param missing.
if [ $# -ne 6 ]; then
	helpUsage
	quitBranchingScript
fi

# Capturing command line parameter as readonly variables. 
readonly CommandLine_projectName=$1
readonly CommandLine_branchVersion=$2
readonly CommandLine_jiraTicket=$3
readonly CommandLine_runType=$4
readonly CommandLine_quietMode=$5
readonly CommandLine_trunkNewVersion=$6

# Define variables. Naming them as static variables as they only need to be defined once 
# and will remain same for script execution life cycle.
readonly Static_ProjectBaseDir=`pwd`
readonly Static_ProjectDir="$Static_ProjectBaseDir/$CommandLine_projectName"
readonly Static_TrunkDirPath="$Static_ProjectDir/$TRUNK_DIR_NAME"
readonly Static_BranchDirPath="$Static_ProjectDir/$BRANCH_DIR_NAME"
readonly Static_BranchVersionPath="$Static_BranchDirPath/$CommandLine_branchVersion"
readonly Static_TrunkSvnPath=$SVN_REPO/$CommandLine_projectName/$TRUNK_DIR_NAME
readonly Static_BranchSvnPath=$SVN_REPO/$CommandLine_projectName/$BRANCH_DIR_NAME/$CommandLine_branchVersion
readonly Static_SvnExternalsDir="$Static_ProjectBaseDir/production"

# Logging Global variables
logHeader "Global Variables"
log "SVN_REPO: $SVN_REPO"
log "TRUNK_DIR_NAME: $TRUNK_DIR_NAME"
log "BRANCH_DIR_NAME: $BRANCH_DIR_NAME"
log "CURRENT_TIME: $NOW"
log "LOG_FILE: $LOG_FILE"
logNewLine

# Logging Command line variables 
# Note: They are also globally accessible within script however calling them command line 
# variable for distinguishing them uniquely. 
logHeader "Command Line Variables"
log "ProjectName: $CommandLine_projectName"
log "BranchVersion: $CommandLine_branchVersion"
log "JiraTicket: $CommandLine_jiraTicket"
log "RunType: $CommandLine_runType"
log "QuietMode: $CommandLine_quietMode"
log "TrunkNewVersion: $CommandLine_trunkNewVersion"
logNewLine

# Logging other variables which are crucial in script execution.
logHeader "Other Variables"
log "ProjectDir: $Static_ProjectDir"
log "TrunkDir: $Static_TrunkDirPath"
log "BranchDir: $Static_BranchDirPath"
log "BranchVersionDir: $Static_BranchVersionPath"
log "SvnExternalsDir: $Static_SvnExternalsDir"
logNewLine

# Verify with dev if he wants to continue at this stage
while true; do
log "Do you want to continue? [Y/N]:"
read opt # Read user input and assign it to variable opt
    case $opt in
        Y|y)
            break
            ;;
        N|n)
            quitBranchingScript
            ;;
        *) echo invalid option;;
    esac
done

# Validating runType parameters
paramValueValidation "runType" "$CommandLine_runType" "${RUN_TYPES[*]}"

# Validating quietMode parameters
paramValueValidation "quietMode" "$CommandLine_quietMode" "${BOOLEAN_TYPES[*]}"

logNewLine
logStatus "Script usages varification... Passed"

# Checking svn repository and validating
validateSvnRepo
return_val=$?

if [ "$return_val" -eq "0" ]; then
	logStatus "svn repository varification... Passed"
else
	logStatus "svn repository varification... Failed."
	quitBranchingScript
fi

# cd shell environment to project directory
logInfo $CommandLine_quietMode "cd $Static_ProjectDir"
cd $Static_ProjectDir

# Checking if $TRUNK_DIR_NAME exist in $Static_ProjectDir
if [ ! -d "$Static_TrunkDirPath" ]; then
	logError "$Static_TrunkDirPath does not exist."
	logStatus "Directory verification... Failed"
	quitBranchingScript
else
	logInfo $CommandLine_quietMode "Checking trunk dir... Found [$Static_TrunkDirPath]"
fi

# Checking if $BRANCH_DIR_NAME exist in $Static_ProjectDir
if [ ! -d "$Static_BranchDirPath" ]; then
	logError "$Static_BranchDirPath does not exist."
	logStatus "Directory verification... Failed"
	quitBranchingScript
else
	logInfo $CommandLine_quietMode "Checking branches dir... Found [$Static_BranchDirPath]"
fi

# Checking if $Static_BranchVersionPath already exist or not.
# Do not Exist: OK, Script will continue
# Exist: Script will quit. This means that provided version has already branched. 
if [ ! -d "$Static_BranchVersionPath" ]; then
	logInfo $CommandLine_quietMode "Checking $Static_BranchVersionPath... Doesn't exist [Passed]"
else
	logError "$Static_BranchVersionPath already exist."
	quitBranchingScript
fi

logStatus "Directory verification... Passed"
	
	
# Execute svn copy command for branching
# Command: svn copy URL_Trunk URL_BranchVersion -m"Message"
logStatus "Creating branch $CommandLine_branchVersion for $CommandLine_projectName"
logInfo $CommandLine_quietMode "svn copy $Static_TrunkSvnPath $Static_BranchSvnPath -m\"$CommandLine_jiraTicket Creating branch $CommandLine_branchVersion for $CommandLine_projectName\""
svn copy $Static_TrunkSvnPath $Static_BranchSvnPath -m"$CommandLine_jiraTicket Creating branch $CommandLine_branchVersion for $CommandLine_projectName"
svnCommandStatus=$?

# Verifying if svn copy command completed successfully. If not than script will quit.
if [ $svnCommandStatus -eq 0 ]; then
	logStatus "Branch $CommandLine_branchVersion created for $CommandLine_projectName"
else
	logError "Branch $CommandLine_branchVersion creation for $CommandLine_projectName failed"
	quitBranchingScript
fi

# Update local svn repository 
logStatus "Updating local svn copy"
executeSvnCommand "$CommandLine_runType" "$CommandLine_quietMode" "$SVN_COMMAND_UP"
svnCommandStatus=$?
# Verifying if svn command completed successfully. If not than script will quit.
if [ $svnCommandStatus -eq 0 ]; then
	logStatus "Update local svn copy completed successfully"
else
	logError "Update local svn copy completed failed"
	quitBranchingScript
fi

#Remove SNAPSHOT from branched pom.xml files
logStatus "Removing SNAPSHOT from branch $CommandLine_branchVersion of $CommandLine_projectName"
logInfo $CommandLine_quietMode "cd $Static_BranchVersionPath"
cd $Static_BranchVersionPath
removeSnapShot $CommandLine_quietMode $Static_BranchVersionPath

#Commit the branch code with change pom.xml
executeSvnCommand "$CommandLine_runType" "$CommandLine_quietMode" "$SVN_COMMAND_CI" "$CommandLine_jiraTicket Removing SNAPSHOT from branch $CommandLine_branchVersion of $CommandLine_projectName"
svnCommandStatus=$?
# Verifying if svn command completed successfully. If not than script will quit.
if [ $svnCommandStatus -eq 0 ]; then
	logStatus "svn commit step completed successfully"
elif [ $svnCommandStatus -eq 100 ]; then
	logStatus "svn commit step ignored"
else
	logError "svn commit step failed"
	quitBranchingScript
fi

#Updating externals
logStatus "Updating externals to $CommandLine_branchVersion for $CommandLine_projectName"
logInfo $CommandLine_quietMode "cd $Static_SvnExternalsDir"
cd $Static_SvnExternalsDir
svn pe svn:externals .
#Commit the externals
executeSvnCommand "$CommandLine_runType" "$CommandLine_quietMode" "$SVN_COMMAND_CI" "$CommandLine_jiraTicket Updating externals for branch $CommandLine_branchVersion of $CommandLine_projectName"
svnCommandStatus=$?
# Verifying if svn command completed successfully. If not than script will quit.
if [ $svnCommandStatus -eq 0 ]; then
	logStatus "svn commit step completed successfully"
elif [ $svnCommandStatus -eq 100 ]; then
	logStatus "svn commit step ignored"
else
	logError "svn commit step failed"
	quitBranchingScript
fi


#Remove SNAPSHOT from branched pom.xml files
logStatus "Bumpup version in trunk of $CommandLine_projectName"
logInfo $CommandLine_quietMode "cd $Static_TrunkDirPath"
cd $Static_TrunkDirPath
bumpUpVersion $CommandLine_quietMode $Static_TrunkDirPath $CommandLine_trunkNewVersion $CommandLine_branchVersion

echo "I am done till here"

#cd to branch directory
#find all poms
#remove all snapshots
# ------
# More to come here
echo "Doing something here"
# ------
